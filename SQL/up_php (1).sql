-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2021 at 07:00 AM
-- Server version: 8.0.24
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `up_php`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `uid` int NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`uid`, `name`, `description`) VALUES
(1, 'HOT COFFEE', 'dsc 1'),
(2, 'ICED COFFEE', 'dsc 2'),
(3, 'FRAPPE COFFEE', 'dsc 3'),
(8, 'SODA', 'hello'),
(9, 'MILK TEA', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

CREATE TABLE `tbl_post` (
  `uid` int NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `content` text NOT NULL,
  `picture` varchar(250) NOT NULL,
  `category` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `tbl_post`
--

INSERT INTO `tbl_post` (`uid`, `title`, `description`, `content`, `picture`, `category`) VALUES
(1, 'អ៊ិចប្រេសសូក្ដៅ', 'Hot Expresso', '', '1625847599Hot Expresso.png', 1),
(2, 'ឡាតេក្តៅ ', 'Hot Latte', '', '1625847838Hot Americano.jpg', 1),
(3, 'កាពូជីណូក្ដៅ', 'Hot Cappuccino ', '', '1625847925Hot Cappuccino.jpg', 1),
(4, 'ម៉ូកាក្ដៅ', 'Hot Mocha', '', '1625848017Hot Mocha.jpg', 1),
(5, 'អាមេរិចកាណូទឹកកក', 'Iced Americano', '', '1625848089Iced Americano.jpg', 2),
(6, 'ខារ៉ាមែលឡាតេទឹកកក', 'Iced Caramel Latte', '', '1625848161Iced Caramel Latte.jpg', 2),
(7, 'ម៉ូកាទឹកកក', 'Iced Mocha', '', '1625848267ced Mocha.jpg', 2),
(8, 'តែបៃតងទឹកកក', 'Iced Green Tea', '', '1625848335iced green tea.jpg', 2),
(9, 'សូកូឡាទឹកកក', 'Iced Chocolate', '', '1625848458ced Chocolate.jpg', 2),
(11, 'ម៉ូកាក្រឡុក', 'Mocha Frappe', '', '1625848628Mocha Frappe.jpg', 3),
(12, 'តែបៃតងក្រឡុក', 'Green Tea Frappe', '', '1625848696green tea Frappe.jpg', 3),
(13, 'វ៉ាន់នីឡាស្រ្ដប៊ឺរីក្រឡុក', 'Vanilla Strawberry Frappe', '', '1625848891Vanilla Stawberry Frappe.jpg', 3),
(14, 'ស្រ្ដប៊ឺរីសូដា', 'Strawberry Soda', '', '1625849143Stawbery Soda.jpg', 8),
(15, 'ប្លូប៊ឺរីសូដា', 'Blueberry Soda', '', '1625849447blueberry soda.jpg', 8),
(16, 'ប៉ែសសូដា', 'Peach Soda', '', '1625849553peach Soda.jpg', 8),
(44, 'តែខ្មៅទឹកដោះគោ', 'Black MIlk Tea', '', '1626195563black milk tea.png', 9),
(46, 'តែខ្មៅទឹកដោះគោ', 'Black MIlk Tea', '', '1626195801black milk tea.png', 9),
(48, 'អ៊ិចប្រេសសូក្ដៅ', 'Milk Tea Green', '', '1626196136milktea 1.jpg', 9),
(49, 'ឡាតេក្តៅ ', 'Chocolate tea', '', '1626196161milktea2.jpg', 9),
(50, 'តែបៃតង', 'Yellow Milk Tea', '', '1626196207milktea3.png', 9),
(51, 'ម៉ូកាទឹកកក', 'Rose Milk Tea', '', '1626196251milktea4.jpg', 9),
(52, 'អ៊ិចប្រេសសូក្ដៅ', 'Bubble Milk Tea', '', '1626196274milktea5.jpg', 9),
(53, 'ម៉ូកាទឹកកក', 'Brown Suger Milk Tea', '', '1626196321milktea4.jpg', 9),
(54, 'អ៊ិចប្រេសសូក្ដៅ', 'Hot Expresso', '', '1625847599Hot Expresso.png', 1),
(55, 'ឡាតេក្តៅ ', 'Hot Latte', '', '1625847838Hot Americano.jpg', 1),
(56, 'កាពូជីណូក្ដៅ', 'Hot Cappuccino ', '', '1625847925Hot Cappuccino.jpg', 1),
(57, 'ម៉ូកាក្ដៅ', 'Hot Mocha', '', '1625848017Hot Mocha.jpg', 1),
(58, 'អាមេរិចកាណូទឹកកក', 'Iced Americano', '', '1625848089Iced Americano.jpg', 2),
(59, 'ខារ៉ាមែលឡាតេទឹកកក', 'Iced Caramel Latte', '', '1625848161Iced Caramel Latte.jpg', 2),
(60, 'ម៉ូកាទឹកកក', 'Iced Mocha', '', '1625848267ced Mocha.jpg', 2),
(61, 'តែបៃតងទឹកកក', 'Iced Green Tea', '', '1625848335iced green tea.jpg', 2),
(62, 'សូកូឡាទឹកកក', 'Iced Chocolate', '', '1625848458ced Chocolate.jpg', 2),
(63, 'សូកូឡាក្រឡុក', 'Chocolate Frappe', '', '1625848561Chocolate Frappe.jpg', 3),
(64, 'ម៉ូកាក្រឡុក', 'Mocha Frappe', '', '1625848628Mocha Frappe.jpg', 3),
(65, 'តែបៃតងក្រឡុក', 'Green Tea Frappe', '', '1625848696green tea Frappe.jpg', 3),
(66, 'វ៉ាន់នីឡាស្រ្ដប៊ឺរីក្រឡុក', 'Vanilla Strawberry Frappe', '', '1625848891Vanilla Stawberry Frappe.jpg', 3),
(67, 'ស្រ្ដប៊ឺរីសូដា', 'Strawberry Soda', '', '1625849143Stawbery Soda.jpg', 8),
(68, 'ប្លូប៊ឺរីសូដា', 'Blueberry Soda', '', '1625849447blueberry soda.jpg', 8),
(69, 'ប៉ែសសូដា', 'Peach Soda', '', '1625849553peach Soda.jpg', 8),
(70, 'តែខ្មៅទឹកដោះគោ', 'Black MIlk Tea', '', '1626195563black milk tea.png', 9),
(71, 'តែខ្មៅទឹកដោះគោ', 'Black MIlk Tea', '', '1626195801black milk tea.png', 9),
(72, 'អ៊ិចប្រេសសូក្ដៅ', 'Milk Tea Green', '', '1626196136milktea 1.jpg', 9),
(73, 'ឡាតេក្តៅ ', 'Chocolate tea', '', '1626196161milktea2.jpg', 9),
(74, 'តែបៃតង', 'Yellow Milk Tea', '', '1626196207milktea3.png', 9),
(75, 'ម៉ូកាទឹកកក', 'Rose Milk Tea', '', '1626196251milktea4.jpg', 9),
(76, 'អ៊ិចប្រេសសូក្ដៅ', 'Bubble Milk Tea', '', '1626196274milktea5.jpg', 9),
(77, 'ម៉ូកាទឹកកក', 'Brown Suger Milk Tea', '', '1626196321milktea4.jpg', 9);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `uid` int NOT NULL,
  `username` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `password` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cpassword` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`uid`, `username`, `password`, `cpassword`) VALUES
(22, 'chhun', '1234', NULL),
(23, 'chhun1', '12', NULL),
(24, 'chhun99', '123', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD PRIMARY KEY (`uid`),
  ADD KEY `fk_category` (`category`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `uid` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tbl_post`
--
ALTER TABLE `tbl_post`
  MODIFY `uid` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `uid` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD CONSTRAINT `fk_category` FOREIGN KEY (`category`) REFERENCES `tbl_category` (`uid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
